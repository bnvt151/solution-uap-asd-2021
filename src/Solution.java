//SEMOGA TIDAK ERROR LAGI

import java.util.*;

public class Solution {
    static int Counter = 0;

    //Ascending sort pake ASCII
    public static Stack<Character> sortStack(Stack<Character> stack) {
        Stack<Character> newStack = new Stack<Character>();
        while (!stack.isEmpty()) {
            char tmp = stack.pop();
            while (!newStack.isEmpty() && newStack.peek() > tmp) {
                stack.push(newStack.pop());
            }
            newStack.push((tmp));
        }
        return newStack;
    }

    //Method buat cek balance
    public static boolean checkValid(String s) {
        Stack<Character> st = new Stack<>();
        for(int i = 0; i < s.length(); i++) {
            Character ch = s.charAt(i);
            if(!st.isEmpty() && ch == '}' && st.peek()=='{') {
                st.pop();
                Counter++;
            } else if(!st.isEmpty() && ch == ')' && st.peek() == '(') {
                st.pop();
                Counter++;
            } else if(!st.isEmpty() && ch == ']' && st.peek() == '[') {
                st.pop();
                Counter++;
            } else {
                st.push(ch);
            }
        }

        //Iterasi 1 kali kalo masih ada sisa (unbalanced)
        //Buat ngecek potensi valid
        if (!st.isEmpty()){

            //Buat stack baru terus di sort pake method sebelumnya
            Stack<Character> tempstack  = new Stack<>();
            tempstack = sortStack(st);

            //Ubah jadi string hilangkan format array
            String str = tempstack.toString().replace(" ", "").replace(",","");

            //Stack baru buat nge-popin yang udah balance
            Stack<Character> newstack  = new Stack<>();

            //Start iterasi dari 1 sampai panjang string -1 biar format kurung siku dari array gak kebaca
            for(int i = 1; i < str.length()-1; i++) {
                Character a = str.charAt(i);
                if(!newstack.isEmpty() && a == '}' && newstack.peek()=='{') {
                    newstack.pop();
                    Counter++;
                } else if(!newstack.isEmpty() && a == ')' && newstack.peek() == '(') {
                    newstack.pop();
                    Counter++;
                } else if(!newstack.isEmpty() && a == ']' && newstack.peek() == '[') {
                    newstack.pop();
                    Counter++;
                } else {
                    newstack.push(a);
                }
            }
            return false;
        }

        if(st.isEmpty()) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        if (checkValid(input)==true){
            System.out.println("Seimbang " + Counter);
        } else {
            System.out.println("Tidak Seimbang " + Counter);
        }
    }
}