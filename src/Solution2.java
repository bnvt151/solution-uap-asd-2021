//HITUNG ANAK DAN DESCENDANT

import java.util.*;

public class Solution2 {
    //DILARANG MENGUBAH BAGIAN INI
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        Tree<Integer> tree = new Tree<>();
        LinkedList<Integer>temp = new LinkedList<>();
        int count =0; //Gapaham ini buat apa, soalnya gakepake
        for (int i = 0; i < n; i++) {
            int a = input.nextInt();
            temp.addLast(a);
            if(tree.isEmpty()){
                tree = new Tree(a);
            }else{
                int j =0;
                boolean masuk;
                do{
                    masuk = tree.add(temp.get(j),a);
                    j++;
                }while(!masuk && j < temp.size());
            }
        }
        int t = input.nextInt();
        System.out.println("Banyak child dari node "+t+": "+tree.hitungChild(t));
        System.out.println("Banyak descendant dari node "+t+": "+tree.hitungDescendant(t));
    }
}
//DILARANG MENGUBAH BAGIAN INI
class Node<T> {
    T data;
    List<Node> childs;
    public Node(T data){
        this.data = data;
        childs = new LinkedList<Node>();
    }
}

class Tree<T>{
    Node <T> root;
    public Tree(){
        this.root = null;
    }
    public Tree(T data){
        root = new Node<T>(data);
    }
    public Tree(Node<T> root){
        this.root = root;
    }
    public boolean isEmpty(){
        return root == null;
    }

    //Method untuk mencari node berdasarkan datanya. DILARANG MENGUBAH BAGIAN INI
    public Node getNode(Node<T> node, T data){
        if (node.data == data) return node;

        for(Object currNode : node.childs){
            Node<T> returnNode = getNode((Node) currNode, data);
            if(returnNode!=null)return returnNode;
        }
        return null;
    }

    //Method buat nambah data ke generic tree
    public boolean add(T dataParent, T data){
        Node<T> node = getNode(root, dataParent);
        if(node != null){
            int jml = 1;

            //Iterasi setiap child dalam suatu node
            for(Node<Integer> temp : node.childs){
                jml += temp.data;
            }

            //Nambahin data setiap nilai childnya kurang dari 2x nilai node
            if(jml <= (int) node.data * 2){
                node.childs.add(new Node<T>(data));
                return true;
            }
        }
        return false;
    }

    //Buat ngehitung jumlah child
    public int hitungChild(T data){
        Node<T> node = getNode(root, data);
        if (node == null)
            return 0;
        else
        return node.childs.size();
    }

    //Buat ngehitung descendantnya secara rekursif
    int counter = 0;
    public int hitungNodeSubTree(Node data){
        if(!data.childs.isEmpty()){
            Iterator<Node> list = data.childs.iterator();
            while(list.hasNext()){
                Node node = list.next();
                counter++;
                hitungNodeSubTree(node);
            }
        }
        return counter;
    }

    //Method yang dipanggil di main
    public int hitungDescendant(T data){
        Node<T> node = getNode(root, data);
        return hitungNodeSubTree(node);
    }
}